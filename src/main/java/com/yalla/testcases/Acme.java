package com.yalla.testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Acme {
	@Test
	public void acmeTest() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://acme-test.uipath.com/account/login");
		
		driver.findElementById("email").sendKeys("suvethamca@gmail.com");
		driver.findElementById("password").sendKeys("Parikumar@1");
		driver.findElementById("buttonLogin").click();
		Thread.sleep(2000);
		WebElement findElementByLinkText = driver.findElementByXPath("(//div[@class='dropdown']/button)[4]");
		Actions mouseover = new Actions(driver);
		mouseover.moveToElement(findElementByLinkText).perform();
		driver.findElementByLinkText("Search for Vendor").click();
		driver.findElementById("vendorTaxID").sendKeys("RO254678");
		driver.findElementById("buttonSearch").click();
		WebElement findElementById = driver.findElementByXPath("((//table[@class='table']/tbody/tr)[2]/td)[1]");
        String text = findElementById.getText();
		
        System.out.println(text);
	}

}
