package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "CreateLead";
		author = "suvetha";
		category = "smoke";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData") 
	public void createLead(String uName, String uPassword,String companyName, String firstName, String lastName) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(uPassword)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCompany(companyName)
		.enterFirstWord(firstName)
		.enterLastWord(lastName)
		.clickCreateLeadButton()
		.verifyFirstName(firstName)
		.clickViewLeadLogout();
		
	}
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	







