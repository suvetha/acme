package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class MyLeadsPages extends Annotations{ 

	public MyLeadsPages() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement eleCreateLeads;
	@And("Click the CreateLead button")
	public CreateLeadsPage clickCreateLead() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleCreateLeads);  
		return new CreateLeadsPage();

	}
	@FindBy(how=How.XPATH, using="//a[text()='Find Leads']") WebElement eleFindLeads;
	public FindLeadPage clickFindLead() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleFindLeads);  
		return new FindLeadPage();

	}

}







