package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class CreateLeadsPage extends Annotations{ 

	public CreateLeadsPage() {
       PageFactory.initElements(driver, this);
	} 
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.XPATH, using="//input[@name = 'submitButton']") WebElement eleCreateLeadButton;
	@And("Enter the Companyname as (.*)")
	public CreateLeadsPage enterCompany(String data) {
		
		clearAndType(eleCompanyName, data);  
		return this; 
	}
	@And("Enter the FirstName as (.*)")
	public CreateLeadsPage enterFirstWord(String data) {
		//WebElement elePassWord = locateElement("id", "password");
		clearAndType(eleFirstName, data); 
		return this; 
	}
    @And("Enter the LastName as (.*)") 
	public CreateLeadsPage enterLastWord(String data) {
		//WebElement elePassWord = locateElement("id", "password");
		clearAndType(eleLastName, data); 
		return this; 
	}
	@And("Click Create Lead button")
	public ViewLeadPage clickCreateLeadButton() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleCreateLeadButton);  
		return new ViewLeadPage();

	}
	
	@Then("Verify Lead is created")
	public void verifyLeadIsCreated() {
		
		System.out.println("Lead is created");
	    
	}

}







