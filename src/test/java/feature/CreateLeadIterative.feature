Feature: Create Lead for Leaftap
Scenario Outline: Positive Create Lead flow
And Enter the username as <username>
And Enter the password as <password>
And Click on the login button
And Click the CRMSFA link
And Click the Leads link
And Click the CreateLead button
And Enter the Companyname as <Companyname>
And Enter the FirstName as <firstname>
And Enter the LastName as <lastname>
When Click Create Lead button
Then Verify Lead is created
Examples: 
|username|password|Companyname|firstname|lastname|
|DemoSalesManager|crmsfa|TCS|Suvetha|vasanthakumar|